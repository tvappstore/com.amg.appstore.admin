var Request = function()
{
    this.getParameter = function( name )
    {
        var rtnval = "";
        var nowAddress = unescape(location.href);
        var parameters = (nowAddress.slice(nowAddress.indexOf("?")+1,nowAddress.length)).split("&");


        for(var i = 0 ; i < parameters.length ; i++)
        {
            var varName = parameters[i].split("=")[0];
            if(varName.toUpperCase() == name.toUpperCase())
            {
                rtnval = parameters[i].split("=")[1];
                break;
            }
        }
        return rtnval;
    };
};

function setFileName(inputFileName, fileName) {
	var fileUrl = fileName.replace("C:\\fakepath\\", "");
	var idFileName = inputFileName;
	
	$(idFileName).val(fileUrl);
}

function encodeValue() {
	var encodeKeywords = encodeURIComponent($('#keywords').val());
	var encodeDeviceModel = encodeURIComponent($('#deviceModel').val());
	var encodeCallsign = encodeURIComponent($('#callsign').val());
	var encodeName = encodeURIComponent($('#name').val());
	var encodeDescription = encodeURIComponent($('#description').val());
	var encodeIconUrl = encodeURIComponent($('#iconFileName').val());
	var encodeSnapshotUrl1 = encodeURIComponent($('#snapshotFileName1').val());
	var encodeSnapshotUrl2 = encodeURIComponent($('#snapshotFileName2').val());
	var encodeAppUrl = encodeURIComponent($('#appFileName').val());

	$('#keywords').val(encodeKeywords);
	$('#deviceModel').val(encodeDeviceModel);
	$('#callsign').val(encodeCallsign);
	$('#name').val(encodeName);
	$('#description').val(encodeDescription);
	$('#iconFileName').val(encodeIconUrl);
	$('#snapshotFileName1').val(encodeSnapshotUrl1);
	$('#snapshotFileName2').val(encodeSnapshotUrl2);
	$('#appFileName').val(encodeAppUrl);
}

function goMenu() {
	var acToken = $('#acTokenHidden').val();
	var email = $('#emailHidden').val();
	var url = "menu.html?acToken="+acToken+ "&email=" +email;
	
	$(location).attr('href', url);
}

function goLogOut() {
	var url = "https://www.google.com/accounts/Logout";
	
	$(location).attr('href', url);
}

function validate() {
	var re_num = /^[0-9_\.]{1,16}$/;
	var re_eng_num = /^[a-z|A-Z|0-9_\.\-]{1,30}$/;
	var re_small_eng_num = /^[a-z|0-9_\.\-]{1,30}$/;
	var re_kor_num = /^[a-z|A-Z|0-9|ㄱ-ㅎ|ㅏ-ㅣ|가-힣_\,\-\.]{1,30}$/;
	var	exp = new RegExp ("^(https|HTTPS|http|HTTP)\:\/\/");
	
	var platform	= $('#platform');
	var deviceModel	= $('#deviceModel');
	var callsign	= $('#callsign');
	var name		= $('#name');
	var version		= $('#version');
	var iconFileName	= $('#iconFileName');
	var snapshotFileName1	= $('#snapshotFileName1');
	var snapshotFileName2	= $('#snapshotFileName2');
	var locale		= $('#locale');
	var webUrl		= $('#webUrl');
	var keywords	= $('#keywords');
	var packageName	= $('#packageName');
	var appFileName	= $('#appFileName');
	var mainActivityClass	= $('#mainActivityClass');
	var description	= $('#description');

	var trimName = platform.val().replace(/\s/g, '');
	if(re_kor_num.test(trimName) != true) {
		alert("platform 명을 확인하여 주세요.");
		platform.focus();
		return false;
	}
	
	trimName = deviceModel.val().replace(/\s/g, '');
	if(re_kor_num.test(trimName) != true) {
		alert("deviceModel 명을 확인하여 주세요");
		deviceModel.focus();
		return false;
	}
	
	trimName = callsign.val().replace(/\s/g, '');
	if(re_kor_num.test(trimName) != true) {
		alert("callsign 명을 확인하여 주세요");
		callsign.focus();
		return false;
	}
	
	trimName = name.val().replace(/\s/g, '');
	if(re_kor_num.test(trimName) != true) {
		alert("name 명을 확인하여 주세요");
		name.focus();
		return false;
	}
	
	trimName = version.val().replace(/\s/g, '');
	if(re_num.test(trimName) != true) {
		alert("version 명을 확인하여 주세요");
		version.focus();
		return false;
	}
	
	trimName = iconFileName.val().replace(/\s/g, '');
	if(re_eng_num.test(trimName) != true) {
		alert("iconUrl 명을 확인하여 주세요");
		iconFileName.focus();
		return false;
	}
	
	if(getIconClass(iconFileName.val()) != true) {
		alert("iconUrl 확장자명을 확인하여 주세요");
		iconFileName.focus();
		return false;
	}
	
	trimName = snapshotFileName1.val().replace(/\s/g, '');
	if(re_eng_num.test(trimName) != true) {
		alert("snapshotUrl1 명을 확인하여 주세요");
		snapshotUrl1.focus();
		return false;
	}
	
	if(getIconClass(snapshotFileName1.val()) != true) {
		alert("snapshotUrl1 확장자명을 확인하여 주세요");
		snapshotUrl1.focus();
		return false;
	}
	
	trimName = snapshotFileName2.val().replace(/\s/g, '');
	if(re_eng_num.test(trimName) != true) {
		alert("snapshotUrl2 명을 확인하여 주세요");
		snapshotUrl2.focus();
		return false;
	}
	
	if(getIconClass(snapshotFileName2.val()) != true) {
		alert("snapshotUrl2 확장자명을 확인하여 주세요");
		snapshotUrl2.focus();
		return false;
	}
	
	trimName = locale.val().replace(/\s/g, '');
	if(re_eng_num.test(trimName) != true) {
		alert("locale 명을 확인하여 주세요.");
		locale.focus();
		return false;
	}
	
	trimName = webUrl.val().replace(/\s/g, '');
	if(exp.test(trimName) != true) {
		alert("webUrl 명을 확인하여 주세요.");
		webUrl.focus();
		return false;
	}
	
	trimName = appFileName.val().replace(/\s/g, '');
	if(re_eng_num.test(trimName) != true) {
		alert("appUrl 명을 확인하여 주세요");
		appFileName.focus();
		return false;
	}
	
	if(getApkClass(appFileName.val()) != true) {
		alert("appUrl 확장자명을 확인하여 주세요");
		appFileName.focus();
		return false;
	}
	
	trimName = keywords.val().replace(/\s/g, '');
	if(re_kor_num.test(trimName) != true) {
		alert("keywords 명을 확인하여 주세요");
		keywords.focus();
		return false;
	}
	
	trimName = packageName.val().replace(/\s/g, '');
	if(re_small_eng_num.test(trimName) != true) {
		alert("packageName 명을 확인하여 주세요");
		packageName.focus();
		return false;
	}
	
	trimName = mainActivityClass.val().replace(/\s/g, '');
	if(re_eng_num.test(trimName) != true) {
		alert("mainActivityClass 명을 확인하여 주세요");
		mainActivityClass.focus();
		return false;
	}
	
	trimName = description.val().replace(/\s/g, '');
	if(trimName.length < 5) {
		alert("description 을 5글자 이상 입력하여 주세요");
		description.focus();
		return false;
	}
	
	return true;
}

//content Type 별로 아이콘을 리턴함 
function getIconClass(filename){
	var extension = filename.substring(filename.lastIndexOf('.') + 1);
	extension = extension.toLowerCase();

	if(("gif|jpeg|png|jpg").indexOf(extension) >= 0){
		return true; 
	}
}

function getApkClass(filename){
	var extension = filename.substring(filename.lastIndexOf('.') + 1);
	extension = extension.toLowerCase();

	if(("apk").indexOf(extension) >= 0){
		return true; 
	}
}
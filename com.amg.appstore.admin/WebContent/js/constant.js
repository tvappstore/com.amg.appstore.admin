// Demo Server url
var serverAdminUrl	= "http://dev.appstore.com:8080/server/v1/admin";
var serverUrl 		= "http://dev.appstore.com:8080/server/v1";
var serverUrlApps 	= "http://dev.appstore.com:8080/server/v1/apps";
var menuUrl 		= "http://dev.appstore.com:8080/admin/menu.html";

var CLIENTID    	= "770671392727-titi28t6t49s5kajhpkklbe6589i02bt.apps.googleusercontent.com";
var REDIRECT    	= "http://dev.appstore.com:8080/admin";

/* local Url */
//var serverAdminUrl 	= "http://localhost:8080/server/v1/admin";
//var serverUrl 		= "http://localhost:8080/server/v1";
//var serverUrlApps 	= "http://localhost:8080/server/v1/apps";
//var menuUrl 		= "http://localhost:8080/admin/menu.html";
//
//var CLIENTID    	= "1071951944411-8qbi4nsbdftk37h6gqa2i9u91v03dlmc.apps.googleusercontent.com";
//var REDIRECT    	= "http://localhost:8080/admin";

/*
 * common 변수
 */
var OAUTHURL    =   'https://accounts.google.com/o/oauth2/auth?';
var VALIDURL    =   'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
var SCOPE       =   'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
var LOGOUT      =   'http://accounts.google.com/Logout';
var TYPE        =   'token';
/**
 * Ajax (jQuery)
 * 	uri 			: URL
 *	param 			: param
 *	callback 		: call function
 *	asyncValue		: syn. (true, false)
 *	typeValue		: post, get
 *	dataTypeValue	: xml, html, json, jsonp, script, text
 *	targetId		: targetId
 */
function getAjaxRequest(url, param, callback, asyncValue, typeValue, dataTypeValue, accessToken) {
	var asynchDefaultValue		= 'true';
	var typeDefaultValue		= 'get';
	var dataTypeDefaultValue	= 'json';
					 
	if(typeValue != undefined && typeValue != "") {
		typeDefaultValue 		= typeValue;
	}
	
	if(asyncValue != undefined && asyncValue != "") {
		asynchDefaultValue 		= asyncValue;
	}
	
	if(dataTypeValue != undefined && dataTypeValue != "") {
		dataTypeDefaultValue 	= dataTypeValue;
	}
	
	$.ajax({
	    url					: url,
	    data				: param,
	    type				: typeDefaultValue,
	    async				: asynchDefaultValue,
	    dataType			: dataTypeDefaultValue,
	    error				: failureAlert,
	    success				: callback,
	    beforeSend			: function(xhr) {xhr.setRequestHeader("X-Access-Token", accessToken);}
	});
}

function failureAlert(xhr, textStatus, errorThrown) {
	var obj = $.parseJSON(xhr.responseText);
	
	if(xhr.status === 401){
		alert("401 ::: " +obj.message);
    }else if(xhr.status === 403){
    	alert("403 ::: " +obj.message);
    }else if(xhr.status === 409) {
    	alert("409 ::: " +obj.message);
    }else {
    	alert(obj.message);
    }
}

function getRandom() {
	var random	= Math.random() * 1000000;
	return random;
}